# Node Demo Firmware
## Requirements
* You need to download/get [Arduino Makefile](https://github.com/sudar/Arduino-Makefile)
* When using the Makefile, you need to define
    * `ARDUINO_MAKEFILE`: The location of [Ardunio Makefile](https://github.com/sudar/Arduino-Makefile) (`/path/to/Arduino-Makefile/Arduino.mk`)
    * `MONITOR_PORT`: The port that the Arduino is connected to (`/dev/ttyUSB0`)

## Building
`make clean ARDUINO_MAKEFILE=blah`

`make all ARDUINO_MAKEFILE=blah`

## Flashing
`make upload ARDUINO_MAKEFILE=blah MONITOR_PORT=/dev/blah`

